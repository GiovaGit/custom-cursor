# PixiJS Javascript boilerplate

Boilerplate for [PixiJS](https://pixijs.io/examples) using Javascript and Parcel.

## How to use it

- Copy this repo as raw code into your computer. *Don't clone it, but download manually*.
- Push to your real project repo.
- Have fun!!🎉🎉