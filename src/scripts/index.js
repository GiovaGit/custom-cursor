import { CUSTOM_EVENTS } from "./custom_events";

const cursor = document.querySelector(".cursor");
const cursorWidth = cursor.offsetWidth;
const cursorHeight = cursor.offsetHeight;
const cursorDot = document.querySelector(".cursor-dot");
const cursorDotWidth = cursorDot.offsetWidth;
const cursorDotHeight = cursorDot.offsetHeight;

// Update screen sides width
let leftScreenSide = window.innerWidth * 0.15;
let rightScreenSide = window.innerWidth - window.innerWidth * 0.15;
window.addEventListener("resize", () => {
  leftScreenSide = window.innerWidth * 0.15;
  rightScreenSide = window.innerWidth - window.innerWidth * 0.15;
});

// Move cursor
let pageX = 0;
let pageY = 0;
let lastX = 0;
let lastY = 0;

const initCursor = () => {
  document.addEventListener("mousemove", (e) => {
    const isInLeftScreenSide = e.pageX < leftScreenSide;
    const isInCenterOfScreen = e.pageX >= leftScreenSide && e.pageX <= rightScreenSide;
    const isInRightScreenSide = e.pageX > rightScreenSide;
    const isGrabbing = cursor.classList.contains("cursor-startGrabbing");

    pageX = e.pageX;
    pageY = e.pageY;

    if (isInLeftScreenSide && !isGrabbing) {
      cursor.dispatchEvent(new Event(CUSTOM_EVENTS.CURSOR_ON_THE_LEFT));
    }
    if (isInCenterOfScreen && !isGrabbing) {
      cursor.dispatchEvent(new Event(CUSTOM_EVENTS.CURSOR_IN_THE_CENTER));
    }
    if (isInRightScreenSide && !isGrabbing) {
      cursor.dispatchEvent(new Event(CUSTOM_EVENTS.CURSOR_ON_THE_RIGHT));
    }
  });

  // Stick to magnet or lerp to cursor position
  // document.querySelectorAll(".magnet").forEach((magnet) => {
  // const magnetX = magnet.getBoundingClientRect().left;
  // const magnetY = magnet.getBoundingClientRect().top;
  //
  // if (
  //   Math.abs(e.pageX - magnetX - cursorWidth * 0.25) < 100 &&
  //   Math.abs(e.pageY - magnetY - cursorHeight * 0.25) < 100 &&
  //   !isGrabbing
  // ) {
  //   let stuckX = lerp(e.pageX, magnetX, 1);
  //   let stuckY = lerp(e.pageY, magnetY, 1);
  //   moveCursor(stuckX - cursorWidth * 0.25, stuckY - cursorHeight * 0.25);
  //   cursor.classList.add("cursor-stuck");
  // } else {
  //   lastX = lerp(lastX, e.pageX, 0.4);
  //   lastY = lerp(lastY, e.pageY, 0.4);
  //   moveCursor(lastX - cursorWidth * 0.5, lastY - cursorHeight * 0.5);
  //   cursor.classList.remove("cursor-stuck");
  // }
  // });

  const render = () => {
    const lerp = (a, b, n) => {
      return (1 - n) * a + n * b;
    };

    lastX = lerp(lastX, pageX, 0.1);
    lastY = lerp(lastY, pageY, 0.1);

    cursorDot.setAttribute(
      "style",
      "top: " + (pageY - cursorDotHeight * 0.5) + "px; left: " + (pageX - cursorDotWidth * 0.5) + "px;"
    );

    cursor.setAttribute(
      "style",
      "top: " + (lastY - cursorHeight * 0.5) + "px; left: " + (lastX - cursorWidth * 0.5) + "px;"
    );
    requestAnimationFrame(render);
  };
  requestAnimationFrame(render);
};
initCursor();

// Hover clickable elements
// document.querySelectorAll(".magnet").forEach((magnet) => {
//   magnet.addEventListener("mouseenter", () => cursor.dispatchEvent(customEvents.cursorCanClick));
//   magnet.addEventListener("mouseleave", () => cursor.dispatchEvent(customEvents.cursorCannotClick));
// });

// Start grabbing
document.addEventListener("mousedown", (e) => {
  const isInCenterOfScreen = e.pageX >= leftScreenSide && e.pageX <= rightScreenSide;
  if (isInCenterOfScreen) {
    cursor.dispatchEvent(new Event(CUSTOM_EVENTS.CURSOR_IS_GRABBING));
  }
});

// Stop grabbing
document.addEventListener("mouseup", (e) => {
  const isInLeftScreenSide = e.pageX < leftScreenSide;
  const isInCenterOfScreen = e.pageX >= leftScreenSide && e.pageX <= rightScreenSide;
  const isInRightScreenSide = e.pageX > rightScreenSide;
  const isGrabbing = cursor.classList.contains("cursor-startGrabbing");

  if (isGrabbing) {
    cursor.classList.remove("cursor-startGrabbing");

    if (isInLeftScreenSide) {
      cursor.dispatchEvent(new Event(CUSTOM_EVENTS.CURSOR_ON_THE_LEFT));
    }
    if (isInRightScreenSide) {
      cursor.dispatchEvent(new Event(CUSTOM_EVENTS.CURSOR_ON_THE_RIGHT));
    }
    if (isInCenterOfScreen) {
      cursor.dispatchEvent(new Event(CUSTOM_EVENTS.CURSOR_STOPPED_GRABBING));
    }
  }
});

// Handle classes
// sides of the screen
cursor.addEventListener(CUSTOM_EVENTS.CURSOR_ON_THE_LEFT, () => {
  cursorDot.classList.remove("visible");
  cursorDot.classList.add("hidden");
  cursor.classList.remove("cursor-shrink");
  cursor.classList.add("cursor-grow");
  cursor.classList.add("cursor-leftOfScreen");
  document.body.style.cursor = "none";
});
cursor.addEventListener(CUSTOM_EVENTS.CURSOR_ON_THE_RIGHT, () => {
  cursorDot.classList.remove("visible");
  cursorDot.classList.add("hidden");
  cursor.classList.remove("cursor-shrink");
  cursor.classList.add("cursor-grow");
  cursor.classList.add("cursor-rightOfScreen");
  document.body.style.cursor = "none";
});
cursor.addEventListener(CUSTOM_EVENTS.CURSOR_IN_THE_CENTER, () => {
  cursorDot.classList.remove("hidden");
  cursorDot.classList.add("visible");
  cursor.classList.remove("cursor-leftOfScreen");
  cursor.classList.remove("cursor-rightOfScreen");
  cursor.classList.remove("cursor-grow");
  cursor.classList.add("cursor-shrink");
  document.body.style.cursor = "none";
});

// grabbing
cursor.addEventListener(CUSTOM_EVENTS.CURSOR_IS_GRABBING, () => {
  cursorDot.classList.remove("visible");
  cursorDot.classList.add("hidden");
  cursor.classList.remove("cursor-shrink");
  cursor.classList.add("cursor-grow");
  cursor.classList.remove("cursor-stopGrabbing");
  cursor.classList.add("cursor-startGrabbing");
  document.body.style.cursor = "grabbing";
});

cursor.addEventListener(CUSTOM_EVENTS.CURSOR_STOPPED_GRABBING, () => {
  cursorDot.classList.remove("hidden");
  cursorDot.classList.add("visible");
  cursor.classList.remove("cursor-grow");
  cursor.classList.add("cursor-shrink");
  cursor.classList.add("cursor-stopGrabbing");
  setTimeout(() => {
    cursor.classList.remove("cursor-stopGrabbing");
    console.log("stop grabbing removed");
  }, 400);
  document.body.style.cursor = "none";
});

// hover clickable elements
// cursor.addEventListener("cursorCanClick", () => {
//   cursor.classList.remove("cursor-shrink");
//   cursor.classList.add("cursor-grow");
// });

// cursor.addEventListener("cursorCannotClick", () => {
//   cursor.classList.remove("cursor-grow");
//   cursor.classList.add("cursor-shrink");
// });
